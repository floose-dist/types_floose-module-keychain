import {Framework, Utils} from "floose";
import Module = Framework.Module;
import ComponentConfiguration = Framework.ComponentConfiguration;
import Schema = Utils.Validation.Schema;

export declare enum DriverType {
    MONGODB = "mongodb",
    MYSQL = "mysql"
}

/**
 * CONFIGURATION
 */
export interface KeychainModuleConfig extends ComponentConfiguration {
    driver: DriverType;
    connection: string;
    entity: {
        delete: {
            ttl: number
        }
    };
}
export declare class KeychainModule extends Module {
    readonly configurationValidationSchema: Schema;
    config: KeychainModuleConfig;
    init(config: ComponentConfiguration): Promise<void>;
    run(): Promise<void>;
}